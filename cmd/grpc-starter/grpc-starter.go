package main

import (
	"flag"
	"net"
	"net/http"
	_ "net/http/pprof"

	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
	"google.golang.org/grpc/reflection"

	grpc_starter "grpc-starter"
	pb "grpc-starter/proto"
)

var (
	flagDebug    = flag.Bool("debug", false, "debug mode")
	flagGrpcPort = flag.String("grpc-port", ":8080", "Service bind address")
	flagHTTPPort = flag.String("http-port", ":18080", "Service bind address")
)

func main() {
	flag.Parse()
	if *flagDebug {
		log.SetLevel(log.DebugLevel)
	}
	server, err := grpc_starter.NewGrpcStarterServer()
	if err != nil {
		log.Panic(err)
	}
	defer server.Close()

	serveHTTP(server)
	serveGRPC(server)
}

func serveGRPC(server *grpc_starter.GrpcStarterServer) {
	lis, err := net.Listen("tcp", *flagGrpcPort)
	if err != nil {
		log.WithError(err).Panic("Listen failed")
	}
	defer lis.Close()

	grpcServer := grpc.NewServer(
		grpc.StreamInterceptor(grpc_prometheus.StreamServerInterceptor),
		grpc.UnaryInterceptor(grpc_prometheus.UnaryServerInterceptor),
		grpc.KeepaliveEnforcementPolicy(keepalive.EnforcementPolicy{PermitWithoutStream: true}),
	)
	defer grpcServer.Stop()

	pb.RegisterHealthServer(grpcServer, server)
	grpc_prometheus.Register(grpcServer)
	reflection.Register(grpcServer)

	panic(grpcServer.Serve(lis))
}

// serveHTTP serve for metrics, pprof
func serveHTTP(server *grpc_starter.GrpcStarterServer) {
	prometheus.MustRegister(server)
	grpc_prometheus.EnableHandlingTimeHistogram()
	go func() {
		http.Handle("/metrics", promhttp.Handler())
		panic(http.ListenAndServe(*flagHTTPPort, nil))
	}()
}
