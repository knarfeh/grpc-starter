#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import os
import os.path
import subprocess

parser = argparse.ArgumentParser(description='Build the image')
parser.add_argument('--remove', action='store_true', help='Do removal only')
args = parser.parse_args()

GOSRC = os.path.join(os.getenv('GOPATH'), 'src')
src_root = os.path.join(GOSRC, 'grpc-starter')

def gen(_files):
    subprocess.check_call([
        'protoc', '-I' + src_root, '-I/usr/local/include',
        '-I' + os.path.join(GOSRC, 'github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis'),
        '--go_out=plugins=grpc:' + GOSRC,
        '--grpc-gateway_out=logtostderr=true:' + GOSRC
    ] + _files)

print('[WARN] genproto.py is no longer needed when building')
print('[INFO] use --remove to do removal only')

for root, dirs, files in os.walk(src_root):
    dirs[:] = [d for d in dirs if d not in ['.git', '.idea', 'bazel-bin', 'bazel-genfiles', 'bazel-out', 'bazel-plaform', 'bazel-testlogs', 'vendor', 'node_modules', 'third_party', 'dev-volumes']]
    proto_files = []
    for file_name in files:
        if not args.remove and file_name.endswith('.proto'):
            path = os.path.join(root, file_name)
            proto_files.append(path)
            print('[INFO] GEN', path)
        elif file_name.endswith('.pb.go') or file_name.endswith('.pb.gw.go'):
            print('[INFO] RM ', os.path.join(root, file_name))
            os.remove(os.path.join(root, file_name))
    if proto_files:
        print("proto_file", proto_files)
        gen(proto_files)
