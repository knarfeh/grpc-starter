package events

import (
	"context"

	"github.com/prometheus/client_golang/prometheus"
	pb "grpc-starter/proto"
)

type GrpcStarterServer struct {
}

func NewGrpcStarterServer() (*GrpcStarterServer, error) {
	return &GrpcStarterServer{}, nil
}

func (s *GrpcStarterServer) Close() {
}

func (s *GrpcStarterServer) Ping(ctx context.Context, in *pb.Empty) (*pb.PingResponse, error) {
	resp := &pb.PingResponse{
		Result: "pong",
	}
	return resp, nil
}

// Describe implement prometheus's interface
func (s *GrpcStarterServer) Describe(ch chan<- *prometheus.Desc) {

}

// Collect implement prometheus's interface
func (s *GrpcStarterServer) Collect(ch chan<- prometheus.Metric) {

}
